import { authenticationService } from '../_services/authentication.service';

export default function authHeader() {
    // return authorization header with jwt token
    const currentUser = authenticationService.currentUser();
    if (currentUser && currentUser.token) {
        return { Authorization: `Bearer ${currentUser.token}` };
    } else {
        return {};
    }
}