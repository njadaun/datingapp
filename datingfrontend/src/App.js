import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";
import Navbar from "./components/layout/Navbar";
import Landing from "./components/layout/Landing";
import Register from "./components/RegisterPage/Register";
import Login from "./components/LoginPage/LoginPage";
import ImageUpload from "./components/UploadImage/UploadImage";

class App extends Component {
    render() {
        return (
            <Router>
                <div className="App">
                    <Navbar />
                    <div className="container valign-wrapper">
                    
                    <Route exact path="/" component={Landing} />
                    <Route exact path="/register" component={Register} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/uploadImage" component={ImageUpload} />
                    </div>
                </div>
            </Router>
        );
    }
} export default App;

