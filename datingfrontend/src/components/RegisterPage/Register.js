import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from 'axios';
import { authenticationService } from '../../_services/authentication.service';

class Register extends Component {
    constructor() {
        super();
        this.state = {
            email: "",
            password: "",
            errors: ''
        };
        // redirect to home if already logged in
        if (authenticationService.currentUser()) {
            this.props.history.push('/');
        }

    }
    onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };
    onSubmit = e => {
        e.preventDefault();
        const newUser = {
            email: this.state.email,
            password: this.state.password,
        };
        // axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        axios.post('http://localhost:5000/users/register', newUser)
            .then(user => {
                this.props.history.push('/login');
            }).catch(e => {
                if( e.response === 'undefined' || e.response.status === 402 ){
                    this.setState({errors :  e.response.data.message});
                }
            });

    };
    render() {
        const { errors } = this.state;
        return (
            <div className="container">
                <div className="row">
                    <div className="col s8 offset-s2">

                        <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                            <h4>
                                <b>Register</b> below
                             </h4>
                            <p className="grey-text text-darken-1">
                                Already have an account? <Link to="/login">Log in</Link>
                            </p>
                        </div>
                        <form onSubmit={this.onSubmit}>
                            {
                                this.state.errors && <div className="col s12">
                                    <span className="helper-text" data-error="wrong" data-success="right">{this.state.errors}</span>
                                </div>

                            }
                            <div className="input-field col s12">
                                <input onChange={this.onChange} value={this.state.email}  id="email" type="email" required />
                                <label htmlFor="email">Email</label>
                            </div>
                            <div className="input-field col s12">
                                <input
                                    onChange={this.onChange}
                                    value={this.state.password}
                                    id="password"
                                    type="password"
                                    required
                                />
                                <label htmlFor="password">Password</label>
                            </div>

                            <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                                <button style={{ width: "150px", borderRadius: "3px", letterSpacing: "1.5px", marginTop: "1rem" }} type="submit" className="btn btn-large waves-effect waves-light hoverable blue accent-3" >
                                    Sign up
                                 </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}
export default Register;