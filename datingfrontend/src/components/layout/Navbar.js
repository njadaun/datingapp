import React, { Component } from "react";
import { Link } from "react-router-dom";
import { authenticationService } from '../../_services/authentication.service';

class Navbar extends Component {
    constructor(props) {
        super(props);
        this.state = { checkUser: false }
    }
    componentDidMount() {
        const checkUser = authenticationService.currentUser();
        this.setState({ checkUser: checkUser })
    }
    logout = props => {
        authenticationService.logout();
        this.setState({ checkUser: false })
    }
    render() {
        return (
            <div className="navbar-fixed">
                <nav>
                    <div className="nav-wrapper">
                        <Link to="/" className="brand-logo">Dating App</Link>
                        {
                            this.state.checkUser  ? (<ul className="right hide-on-med-and-down">
                                <li><Link to="/uploadImage">Upload Image</Link></li>
                                <li onClick={this.logout} ><Link to="/login">Logout</Link></li>
                            </ul>
                            )
                            :
                            (<ul className="right hide-on-med-and-down">
                                <li><Link to="/login">Login</Link></li>
                                <li><Link to="/register">Register</Link></li>
                            </ul>
                            )
                        }
                    </div>
                </nav>
            </div>
        );
    }
}
export default Navbar;