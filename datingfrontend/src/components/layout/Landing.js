import React, { Component } from "react";
import axios from 'axios';
import io from 'socket.io-client';
import authHeader from '../../_helpers/auth-header';
import { authenticationService } from '../../_services/authentication.service';
import M from "materialize-css";
class Landing extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: "",
            endpoint: 'http://localhost:5000',
            socket : ''
        };
        // redirect to home if already logged in
        let loggedUser = authenticationService.currentUser();
        if (!loggedUser) {
            this.props.history.push('/login');
        } 
    }
    componentDidMount() {
        let token = authHeader();
        axios.defaults.headers.common['Authorization'] = token.Authorization;
        axios.post('http://localhost:5000/images/getAllImages')
            .then(images => {
                if (images.status === 200) {
                   this.setState({ items: images.data })
                }
            }).catch(e => {
                if( e.response === 'undefined' ||( e.response.status === 401 && e.response.data.message === "Token has expired")){
                    authenticationService.logout();
                    this.props.history.push('/login');
                }
            }); 
                      
            const socket = io(this.state.endpoint,{query: {userId: token.Authorization}});
            this.setState({socket:socket});

            socket.on('liked',data => {
                M.toast({html: data});
            });
    }
    onLike = ( image_id) =>{
        //socket
        this.state.socket.emit('like',{image_user_id:image_id,like:1});
    }
    onSuperLike = (image_id) =>{
        this.state.socket.emit('superlike',{image_user_id:image_id,super_like:1});
    }
    blockuser = (block_user_id) =>{
        let data = {blocked_user_id: block_user_id,status: 1}
        axios.post('http://localhost:5000/users/blockUser',data)
            .then(user => {
                if (user.status === 200) {
                    let filtered =   this.state.items.filter(( value,index)=>{
                        return value._id !== block_user_id;
                    });
                   this.setState({ items: filtered })
                }
            });
    }
    render() {
        let images = this.state.items;
        let items = Object.values(images).map((item, index) => {
            return item.profile_image && <div className="card col s12 m5 offset-m1" key={item._id}>
                <div className="card-image waves-effect waves-block waves-light">
                    <img className="activator" src={`http://localhost:5000/${item.profile_image}`} alt ="User Profile"/>
                </div>
                <div className="card-content">
                    <span className="card-title activator grey-text text-darken-4">{item.email}</span>
                    <p><span className="waves-effect waves-light btn-small"  onClick={(e) => this.onLike(item._id)} >Like</span></p>
                    <p><span className="waves-effect waves-light btn-small" onClick={(e) => this.onSuperLike(item._id)}>Super Like</span></p>
                    <p><span className="waves-effect waves-light btn-small" onClick={(e) => this.blockuser(item._id)}>Block</span></p>
                </div>
                
            </div>
        });
        return (
            <div className="row">
                {
                    items ? items : <span>No Image uploaded by other users</span>
                }
            </div>
        );
    }
} export default Landing