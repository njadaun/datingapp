import React, { Component } from "react";
import { Link } from "react-router-dom";
import { authenticationService } from '../../_services/authentication.service';

import axios from 'axios';


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            errors: ''
        };

        // redirect to home if already logged in
        if (authenticationService.currentUser()) {
            this.props.history.push('/');
        }
    }
    onChange = e => {
        this.setState({ [e.target.id]: e.target.value });
    };
    onSubmit = e => {
        e.preventDefault();
        const userData = {
            email: this.state.email,
            password: this.state.password
        };
        axios.post('http://localhost:5000/users/authenticate', userData)
            .then(user => {
                if (user.status === 200) {
                    localStorage.setItem('currentUser', JSON.stringify(user.data));
                    window.location = '/';
                }
            }).catch(e => {
                if( e.response === 'undefined' || e.response.status === 400 ){
                    this.setState({errors :  e.response.data.message});
                }
            });
    };
    render() {
        const { errors } = this.state;
        return (
            <div className="container">
                <div  className="row">
                    <div className="col s8 offset-s2">

                        <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                            <h4>
                                <b>Login</b> below
                             </h4>
                            <p className="grey-text text-darken-1">
                                Don't have an account? <Link to="/register">Register</Link>
                            </p>
                        </div>
                        <form  onSubmit={this.onSubmit}>
                            {
                             this.state.errors &&   <div className="col s12">
                                    <span className="helper-text" data-error="wrong" data-success="right">{this.state.errors}</span>
                                </div>

                            }
                            <div className="input-field col s12">
                                <input
                                    onChange={this.onChange}
                                    value={this.state.email}
                                    id="email"
                                    type="email"
                                    required
                                />
                                <label htmlFor="email">Email</label>
                            </div>
                            <div className="input-field col s12">
                                <input
                                    onChange={this.onChange}
                                    value={this.state.password}
                                    id="password"
                                    type="password"
                                    required
                                />
                                <label htmlFor="password">Password</label>
                            </div>
                            <div className="col s12" style={{ paddingLeft: "11.250px" }}>
                                <button
                                    style={{
                                        width: "150px",
                                        borderRadius: "3px",
                                        letterSpacing: "1.5px",
                                        marginTop: "1rem"
                                    }}
                                    type="submit"
                                    className="btn btn-large waves-effect waves-light hoverable blue accent-3">
                                    Login
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
} export default Login;