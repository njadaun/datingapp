import React, { Component } from "react";
// import { Link } from "react-router-dom";
import { authenticationService } from '../../_services/authentication.service';
import authHeader from '../../_helpers/auth-header';

import axios from 'axios';


class ImageUpload extends Component {
    constructor(props) {
        super(props);
        
        // redirect to home if already logged in
        let loggedUser = authenticationService.currentUser();
        if (!loggedUser) { 
           this.props.history.push('/');
        }
        this.state = {
            profile: null,
            user_id: loggedUser._id
        };
    }
    onChange = e => {
        this.setState({ profile: e.target.files[0] });
    };
    onSubmit = e => {
        e.preventDefault();
        let token =  authHeader();
        const data = new FormData() 
        data.append('profile',this.state.profile);
        data.append('user_id',this.state.user_id);
        axios.defaults.headers.common['Authorization'] = token.Authorization;
        axios.post('http://localhost:5000/images/upload',data)
        .then(user => {
            if(user.status === 200) {
                this.props.history.push('/');
            }
            
        }).catch(e => {
            if(e.response.status === 401 && e.response.data.message === "Token has expired"){
                authenticationService.logout();
                this.props.history.push('/login');
            }
            
        });       
    };
    render() {
        return (

            <form noValidate onSubmit={this.onSubmit} >
                <div className="file-field input-field">
                    <div className="btn">
                        <span>File</span>
                        <input type="file" onChange={this.onChange} />
                    </div>
                    <div className="file-path-wrapper">
                        <input className="file-path validate" type="text" />
                    </div>
                    <button className="btn waves-effect waves-light" type="submit" name="action">Submit
                        <i className="material-icons right">send</i>
                    </button>
                </div>
            </form>
        )
    }
}export default ImageUpload;