
export const authenticationService = {
    logout,
    currentUser
};

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
}
function currentUser(){
    let user =    JSON.parse(localStorage.getItem('currentUser'));
    return user && user.token ? user : false;
}