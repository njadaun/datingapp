const   mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schema = new Schema({
    user_id: {type: Schema.ObjectId,ref: 'User', required:true },
    image_user_id: {type:Schema.ObjectId,ref: 'User', required:true },
    like: {type: Number,default: 0 },
    super_like: {type: Number,default: 0 },
    createdDate: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('Image', schema);