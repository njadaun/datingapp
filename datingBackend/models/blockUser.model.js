const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const schema = new Schema ({
    user_id : {type:Schema.ObjectId,ref: 'User',required:true},
    blocked_user_id : { type:Schema.ObjectId,ref: 'User', required :true},
    status: { type: Number ,required:true, default:0}
})

schema.set('toJSON',{virtuals:true});

module.exports = mongoose.model('BlockedUser',schema);