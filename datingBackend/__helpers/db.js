const config = require('../config.json');
const mongoose = require('mongoose');
global.ObjectId = mongoose.Types.ObjectId;
mongoose.connect(process.env.MONGODB_URI || config.connectionString, { useCreateIndex: true, useNewUrlParser: true,useUnifiedTopology: true  });
mongoose.Promise = global.Promise;

module.exports = {
    User: require('../models/user.model'),
    Image: require('../models/image.model'),
    BlockedUser : require('../models/blockUser.model')
};