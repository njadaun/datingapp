let io = require('socket.io'),
socketInfo = {};
const jwt = require('jsonwebtoken');
const db = require('../__helpers/db');
const imageService = require('../controllers/images/image.service');

const User = db.User;
exports.connectSocket = function (server) {
    try {
        console.log("server listener---");
        io = io.listen(server);
        io.on('connection',async socket => {
          
            let userID = '';
            if(socket.id){
                userID = getUserIdFromToken(socket.handshake.query.userId);
                socketInfo[userID]=socket.id;
                if(userID) {
                    await User.findOneAndUpdate({ _id:ObjectId(userID)},{$set: {socket_id: socket.id}},{new:true});
                   
                }
                console.log('socket id saved > >>>>',socketInfo)
    
            }else{
                io.emit('error','Socket is not connected')
            }
            // Image like socket
            socket.on('like', function (data) { ////senderId,receiverId,message,messageType
                if (userID && data.image_user_id) {
                    imageService.likeImage(userID, data)
                    .then(async image => {
                        let user_socket = await User.findOne({_id:ObjectId(data.image_user_id)}, {"socket_id":1} );
                        console.log(user_socket,'----------User Socket');
                        
                        io.to(user_socket.socket_id).emit('liked','Your Image has been liked' );
                    }).catch(err=>{
                        console.log(err);
                    });
                } else {
                    console.log("data not in format");
                }
    
            });

            // Image super like socket
            socket.on('superlike',function(data){
                if (userID && data.image_user_id) {
                    imageService.superLikeImage(userID, data)
                    .then(async image => {
                        let user_socket = await User.findOne({_id:ObjectId(data.image_user_id)}, {"socket_id":1,"email":1} );
                        io.to(user_socket.socket_id).emit('liked',`${user_socket.email} has liked your Image` );
                    }).catch(err=>{
                        console.log(err);
                    });
                } else {
                    console.log("data not in format");
                }
            });
    
            socket.on('disconnect', function () {
                console.log('Socket disconnected---->>>>>>>>>',socketInfo);
            
                if (socketInfo.hasOwnProperty(socket.id)) var userId = socketInfo[socket.id];
                if (socketInfo.hasOwnProperty(userId)) delete socketInfo[userId];
                if (socketInfo.hasOwnProperty(socket.id)) delete socketInfo[socket.id];
            
            });
    
        })
    } catch(e) {
        throw e;
    }
}

function getUserIdFromToken(token){
    // verify a token symmetric - synchronous
    token = token.split(' ')[1];
    let decoded = jwt.decode(token, {complete: true})

    return decoded ? decoded.payload.sub : '';
}