// server.js

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors'),
path = require('path');
const http = require('http');
//Port
const PORT = 5000; 

const jwt = require('./__helpers/jwt');
const errorHandler = require('./__helpers/error-handler');
const connectSocket  = require('./__helpers/socketManager');
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());
app.use(express.static('public/uploads'));
// app.use('/static', express.static(path.join(__dirname , 'public/uploads')));

// use JWT auth to secure the api
app.use(jwt());
// user api routes
app.use('/users', require('./controllers/users/'));
// image api routes
app.use('/images', require('./controllers/images'));
// global error handler
app.use(errorHandler);
const server = http.createServer(app);
connectSocket.connectSocket(server);
server.listen(PORT, function(){
   console.log('Server is running on Port',PORT);
});