const config = require('../../config.json');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const db = require('../../__helpers/db');
const User = db.User;
const BlockedUser = db.BlockedUser;
module.exports= {
    authenticate,
    getById,
    create,
    blockUser
};

async function authenticate({ email, password }) {
    const user = await User.findOne({ email });
    if (user && bcrypt.compareSync(password, user.password)) {
        const { password, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ sub: user.id }, config.secret,{ expiresIn: config.tokenLife});
        return {
            ...userWithoutHash,
            token
        };
    }
}



async function getById(id) {
    return await User.findById(id).select('-password');
}

async function create(userParam) {
    // validate
    if (await User.findOne({ email: userParam.email })) {
        throw 'Email "' + userParam.email + '" is already taken.';
    }

    const user = new User(userParam);

    // hash password
    if (userParam.password) {
        user.password = bcrypt.hashSync(userParam.password, 10);
    }
    user.email = userParam.email;
    // save user
    await user.save();
}

async function blockUser(user_id,blockUserParam){
    if( !user_id && !blockUserParam.blocked_user_id && !blockUserParam.status){
        throw "Please Send all parameter";
    }

    let blockedUser = await BlockedUser.findOneAndUpdate({user_id:ObjectId(user_id),blocked_user_id: ObjectId(blockUserParam.blocked_user_id)},
    {status: blockUserParam.status}, {
        upsert: true, new: true, lean: true
    });
    return blockedUser;
}
