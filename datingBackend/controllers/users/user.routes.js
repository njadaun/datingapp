const express = require('express');
const router = express.Router();
const userController = require('./users.controller');

// routes
router.post('/authenticate', userController.authenticate);
router.post('/register', userController.register);
router.post('/blockUser', userController.blockUser);

module.exports = router;