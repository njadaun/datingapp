
const userService = require('./user.service');
const jwt = require('jsonwebtoken');

module.exports = {
    authenticate:authenticate,
    register:register,
    blockUser:blockUser
}

function authenticate(req, res, next) {
    userService.authenticate(req.body)
        .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
        .catch(err => next(err));
}

function register(req, res, next) {
    userService.create(req.body)
        .then(() => res.json({message:"User created"}))
        .catch(err => next(err));
}

function blockUser(req,res,next){
    let user_id = getUserIdFromToken(req.headers.authorization);
    userService.blockUser(user_id,req.body)
        .then(() => res.json({message:"User Blocked"}))
        .catch(err => next(err));
}
function getUserIdFromToken(token){
    // verify a token symmetric - synchronous
    token = token.split(' ')[1];
    let decoded = jwt.decode(token, {complete: true})

    return decoded ? decoded.payload.sub : '';
}