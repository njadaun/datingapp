const db = require('../../__helpers/db');

const Image = db.Image;
const User = db.User;
const BlockedUser = db.BlockedUser;
module.exports = {
    saveImage,
    likeImage,
    superLikeImage,
    getAllImages
};
async function saveImage(req) {
    const file = req.file
    if (!file) {
        throw 'Please upload a file';
    }
    user_id = req.body.user_id;
   
    const user = await User.findById(user_id);
    // validate
    if (!user) throw 'User not found';

    let image = { profile_image: file.filename};
    // copy userParam properties to user
    Object.assign(user, image);

    await user.save();
}

async function likeImage(user_id,imageParam) {
    if (!user_id && !imageParam.image_user_id && !imageParam.like) {
        throw "Please send all parameter";
    }
    let image = await Image.findOneAndUpdate({ user_id: ObjectId(user_id), image_user_id: ObjectId(imageParam.image_user_id) },
    {like: imageParam.like}, {
        upsert: true, new: true, lean: true
    });
    return image;
   
}

async function superLikeImage(user_id,imageParam) {
    if (!user_id && !imageParam.image_user_id && !imageParam.super_like) {
        throw "Please send all parameter";
    }
    let image = await Image.findOneAndUpdate({ user_id: ObjectId(user_id), image_user_id: ObjectId(imageParam.image_user_id) },
    {super_like: imageParam.super_like}, {
        upsert: true, new: true, lean: true
    });
    return image;

}

async function getAllImages(user_id) {
    let data = await Promise.all([
        BlockedUser.find({user_id: ObjectId(user_id)}, {blocked_user_id: 1}, {lean: true}),
        Image.find({user_id: ObjectId(user_id)}, {}, {lean: true})
    ]);
    blockedUsers = data[0].map(obj => obj.blocked_user_id);
    blockedUsers.push(ObjectId(user_id))
    let users = await User.find({_id: {$nin: blockedUsers}},{email: 1, profile_image: 1},{lean:true});
     
    users = users.map(obj => {
        obj.like = 0;
        obj.superLike = 0;
        let image = data[1].find(o => obj._id.toString()=== o.image_user_id.toString())
        
        if(image) {
            if(image.like) obj.like = 1;
            if(image.super_like) obj.superLike = 1; 
        }
        return obj;
    })

    return users;
}


