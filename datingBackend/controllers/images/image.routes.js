const express = require('express');
const router = express.Router();
const imageController = require('./images.controller');
const imageStorage = require('../../__helpers/image.storage');

module.exports = router;

router.post('/upload',imageStorage.single('profile'),imageController.upload);
router.post('/imageLike',imageController.imageLike);
router.post('/imageSuperLike',imageController.imageSuperLike);
router.post('/getAllImages',imageController.getAllImages);
