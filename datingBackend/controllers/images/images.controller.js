const imageService = require('./image.service');
const jwt = require('jsonwebtoken');

module.exports = {
    upload,
    imageLike,
    imageSuperLike,
    getAllImages
}

function upload(req,res,next){
    imageService.saveImage(req)
    .then(() => res.json({message:"Image uploaded"}))
    .catch(err => next(err));
}

function imageLike(req,res,next){
    let user_id = getUserIdFromToken(req.headers.authorization);
    imageService.likeImage(user_id, req.body)
    .then(() => res.json({message:"Image Liked"}))
    .catch(err => next(err));
}
function imageSuperLike(req,res,next){
    let user_id = getUserIdFromToken(req.headers.authorization);
    imageService.superLikeImage(user_id, req.body)
    .then(() => res.json({message:"Image Super Liked"}))
    .catch(err => next(err));
}

function getAllImages(req,res,next){
    let user_id = getUserIdFromToken(req.headers.authorization);
    imageService.getAllImages(user_id)
    .then(images => images ? res.json(images) : res.status(400).json({ message: 'No Images found' }))
    .catch(err => next(err));
}

function getUserIdFromToken(token){
    // verify a token symmetric - synchronous
    token = token.split(' ')[1];
    let decoded = jwt.decode(token, {complete: true})

    return decoded ? decoded.payload.sub : '';
}